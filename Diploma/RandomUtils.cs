﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class RandomUtils : MonoBehaviour
{
    private static System.Random _rng = new System.Random();

    public static int Random(int number)
    {
        return _rng.Next(number);
    }
    public static T RandomEnumValue<T>()
    {
        var v = System.Enum.GetValues(typeof(T));
        return (T)v.GetValue(_rng.Next(v.Length));
    }
}

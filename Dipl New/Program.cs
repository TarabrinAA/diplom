﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickGraph;

namespace Dipl_New
{
    enum Tags { A, B, C, D, Opponent, PlacesToFacility, Consumer}
    class Program
    {
        static void Main(string[] args)
        {
            var graph = new Graph();
            do
            {
            graph = new Graph(10, -10, 10);
            graph.Print();
            Console.WriteLine();
            } while (graph.CheckConnectivity()!=1);
           
            graph.PrintShortestPath(0, 3);

        }
    }
}

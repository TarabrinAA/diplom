﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickGraph;

namespace Dipl_New
{
    public class Coords
    {
        public double x, y;
        public Coords(Coords anotherCoords)
        {
            this.x = anotherCoords.x;
            this.y = anotherCoords.y;
        }
        public Coords(int min,int max)
        {
            this.x=RandomUtils._rng.Next(min,max);
            this.y=RandomUtils._rng.Next(min,max);
        }
        public Coords(double x, double y)
        {

                this.x = x;
                this.y = y;
            
        }
        public static double operator ^(Coords first, Coords second)
        {
            return Math.Sqrt(Math.Pow(first.x - second.x, 2) + Math.Pow(first.y - second.y, 2));
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dipl_New
{
    class RandomUtils
    {
        public static System.Random _rng = new System.Random();

        public static int Random(int number)
        {
            return _rng.Next(number);
        }
        public static T RandomEnumValue<T>()
        {
            var v = System.Enum.GetValues(typeof(T));
            return (T)v.GetValue(_rng.Next(v.Length));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickGraph;
using QuickGraph.Algorithms;
using QuickGraph.Graphviz;
using System.Numerics;


namespace Dipl_New
{
    class Graph
    {
        private AdjacencyGraph<int, Edge<int>> _graph;
        private Dictionary<Edge<int>, double> _costs;
        private List<Vector2> _coords;
        private List<Tags> _tags;
        public Graph()
        { }
        public Graph(int k, int min, int max)
        {
            SetUpEdgesAndCosts();

            for (int i = 0; i < k; i++)
            {
                _coords.Add(GetCoords(min, max));
                _tags.Add(RandomUtils.RandomEnumValue<Tags>());
            }

            for (int i = 0; i < k; i++)
            {
                for (int j = i + 1; j < k; j++)
                {
                    if (RandomUtils.Random(3) == 0)
                    {
                        float distance = Vector2.Distance(_coords[i], _coords[j]);
                        double newDistance = RandomUtils.Random((int)(distance) % 10) + (distance);
                        AddEdgeWithCosts(i, j, newDistance);
                        AddEdgeWithCosts(j, i, newDistance);
                    }
                }
            }
        }

        private Vector2 GetCoords(int min, int max)
        {
            return new Vector2(RandomUtils._rng.Next(min, max), RandomUtils._rng.Next(min, max));
        }

        public int CheckConnectivity()
        {
            IDictionary<int, int> components = new Dictionary<int, int>();
            int componentCount = _graph.StronglyConnectedComponents<int, Edge<int>>(out components);
            Console.WriteLine("Graph contains {0} strongly connected components", componentCount);
            if (componentCount > 1)
            {
                Console.WriteLine("Graph is not strongly connected");
            }
            return componentCount;
        }

        public void Print()
        {
            int i = 0;
            foreach (var v in _graph.Vertices)
            {
                Console.WriteLine("\n\n({0},{1})", _coords[i].X, _coords[i].Y);
                Console.Write("{0} ", _tags[i]);
                i++;
                Console.WriteLine();
                foreach (var e in _graph.OutEdges(v))
                    Console.Write(" {0}= {1}|", e, _costs[e]);
            }
        }

        public void SetUpEdgesAndCosts()
        {
            _graph = new AdjacencyGraph<int, Edge<int>>();
            _costs = new Dictionary<Edge<int>, double>();
            _coords = new List<Vector2>();
            _tags = new List<Tags>();
        }
        public double GetPathLength(IEnumerable<Edge<int>> path)
        {
            double result = 0;
            foreach (var edge in path)
            {
                result += _costs[edge];
            }
            return result;
        }
        public void PrintShortestPath(int @from, int to)
        {
            var edgeCost = AlgorithmExtensions.GetIndexer(_costs);
            var tryGetPath = _graph.ShortestPathsDijkstra(edgeCost, @from);
            //var tryGetpath = _graph.ShortestPathsAStar(edgeCost,);

            IEnumerable<Edge<int>> path;
            if (tryGetPath(to, out path))
            {
                PrintPath(@from, to, path);
                Console.WriteLine(GetPathLength(path));
            }
            else
            {
                Console.WriteLine("No path found from {0} to {1}.");
            }
        }
        private static void PrintPath(int @from, int to, IEnumerable<Edge<int>> path)
        {
            Console.Write("Path found from {0} to {1}: {0}", @from, to);
            foreach (var e in path)
                Console.Write(" > {0}", e.Target);
            Console.WriteLine();
        }
        private void AddEdgeWithCosts(int source, int target, double cost)
        {
            var edge = new Edge<int>(source, target);
            _graph.AddVerticesAndEdge(edge);
            _costs.Add(edge, cost);
        }
    }
}
